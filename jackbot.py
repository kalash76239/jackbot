import discord
import startgame
import gameList
import sendCommand

client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('!jackbot'):
        await message.channel.send('Hello!')
        await message.channel.send(message.content)

        if (message.content.find("listGames") != -1):
            gameListPretty = gameList.getPrettifiedGameList()
            await message.channel.send(gameListPretty)
            return

        if (message.content.find("startGame") != -1):
            try:
                gameToStart = message.content.split(" ")[2]
                await message.channel.send("attempting to start " + gameToStart)
                startGameMsg = startgame.startGameCMD(gameToStart)
                await message.channel.send(startGameMsg)
            except:
                print("failed to start game")
            return
            
        if (message.content.find("key") != -1):
            keyCommand = message.content.split(" ")[2]
            await message.channel.send("attempting to press " + keyCommand)
            sendMsg = sendCommand.send(keyCommand)
            await message.channel.send(sendMsg)
            return



def getGamesAndParentExe():
    gamesList = gameList.getGamesList()
    returnMessage = "The games list is "


client.run('token goes here')