import pyautogui
import sys

class GameInfo:
    parentGame = ''
    name = ''
    xCoordinate = 0
    yCoordinate = 0


def getGamesList():
    gameList = []
    with open('gameConfig.txt') as f:
        for line in f:
            try:
                gInfo = GameInfo()
                gInfo.parentGame = line.split(',')[0]
                gInfo.name = line.split(',')[1]
                gInfo.xCoordinate = line.split(',')[2]
                gInfo.yCoordinate = line.split(',')[3]
                gameList.append(gInfo)
            except:
                print("error adding <start>'", line, "</start>'")
    return gameList

def checkForSysArgs():
    try:
        return sys.argv[1]
    except:
        return False

def getGameInfo(gameList, gameName):
    for x in range(0, len(gameList)):
        if gameList[x].name.lower() == gameName.lower():
            return gameList[x]
    return False

def getPrettifiedGameList():
    gamesList = getGamesList()
    returnStatement = "Here is a list of games along with their executable available\n parentGameContainer => actual game Name\n Ergo jackbox 1 => drawful " 
    returnStatement = returnStatement + "\n"
    for x in range(0, len(gamesList)):
        returnStatement = returnStatement + gamesList[x].parentGame + " => " + gamesList[x].name + "\n"
    return returnStatement

# gameList = getGamesList()
# for x in range(0, len(gameList)):
#     print(gameList[x].name)

# whatGameToPlay = checkForSysArgs()
# if whatGameToPlay:
#     print (whatGameToPlay, "<- that's what game we will be playing")
#     gameInfo = getGameInfo(gameList, whatGameToPlay)
#     if(gameInfo):
#         print("the game we will be playing is ", gameInfo.name)
#         print("if we had a mouse we'd click", gameInfo.xCoordinate, gameInfo.yCoordinate)
#         print("And if we were to start this game up it'd be located in the game", gameInfo.parentGame)
#         print("so we're gonna click it and see what happens lol")
#         pyautogui.click(int(gameInfo.xCoordinate), int(gameInfo.yCoordinate))
# else:
#     print("The games available are")
#     for x in range(0, len(gameList)):
#         print(gameList[x].parentGame, ":", gameList[x].name)




