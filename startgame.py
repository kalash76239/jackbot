import sys
import os
class Game:
    gameName = ''
    gamePath = ''

def getGameInfo():
    gameFileList = []
    with open('gameFileList.txt') as f:
        for line in f:
            try:
                gameFile = Game()
                gameFile.gameName = line.split(',')[0]
                gameFile.gamePath = line.split(',')[1]
                gameFileList.append(gameFile)
            except:
                print(line, "<= failed to be added to gamefile")
    return gameFileList

def getGameByName(gameInfoArray, gameName):
    for x in range(0, len(gameInfoArray)):
        if (gameInfoArray[x].gameName.lower() == gameName):
            return gameInfoArray[x]
    return False
        

def checkForSysArgs():
    try:
        return sys.argv[1]
    except:
        return False    


def startGame():
    whatGametoStart = checkForSysArgs()
    if (whatGametoStart):
        gameInfo = getGameInfo()
        game = getGameByName(gameInfo, whatGametoStart)
        if (game):
            print (game.gameName, ":", game.gamePath)
            os.system(game.gamePath)
            return True
        else:
            return False
    return False

def startGameCMD(gameName):
    gameInfo = getGameInfo()
    print(gameName)
    game = getGameByName(gameInfo, gameName)
    if (game):
        print (game.gameName, ":", game.gamePath)
        os.system(game.gamePath)
        return "starting"
    else:
        return "game not available"

# if not (startGame()):
#     gameInfoList = getGameInfo()
#     for x in range(0, len(gameInfoList)):
#         print (gameInfoList[x].gameName)
    

