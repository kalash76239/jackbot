import pyautogui
import time
import sys

# def checkForSysArgs():
#     try:
#         return sys.argv[1]
#     except:
#         return False


def send(argument):
    argList = ["esc", "enter", "down", "up", "left", "right"]
    if (argument):
        for x in range(0, len(argList)):
            if argument == argList[x]:
                pyautogui.click(350, 350)
                pyautogui.press(argument)
                return "successfully pressed " + argument
        if(argument == "help"):
            returnHelp = "you can use the following commands to control the keyboard\n"
            for x in range(0, len(argList)):
                returnHelp = returnHelp + "!jackbot " + "key " + argList[x] + "\n"
            return returnHelp





