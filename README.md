# Jackbot

Jackbot has 4 python files and 2 text files: below I will explain their purpose

#jackbot.py
this is the bot that interacts with discord. if you're running this the first time you need to grab yourself a discord bot. 

#gameList.py 
This gets a list of available games (as stored in gameConfig.txt)
It's more of a later stretch feature; but there's an idea that later on we 
could have the bot start games themselves beyond just remote keyboard control which is why we have the x and y coordinates listed (default is 150, 150)


#startgame.py 
this program starts games that are listed in gameFileList.txt by name in the startGameCMD method. 
also displays a list of available games. in the case of this we're referring to the jackbox party packs since those are treaed by steam
as games even though they're game containers. 

#sendCommand.py
this literally sends commands to wherever the program is running; 
atm we only allow esc, enter, down, up, left, right and these should stay limited as to prevent people from fucking with the bot too much. 


#gameConfig.txt
This file lists the pack the game is in, the game name, the x coordinate of the game on the menu screen and the y coordinate. 
the x and y coordinates are stretch goals but eventually in theory we should be able to eventually start the game without needing to send commands to navigate the screen.
